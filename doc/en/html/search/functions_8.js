var searchData=
[
  ['setback',['setBack',['../class_grid_widget.html#a64cd0418127923278bd7dd3ddd3bd459',1,'GridWidget']]],
  ['setcellsize',['setCellSize',['../class_grid_widget.html#a8a128f08a3471f751442dfe1306250f4',1,'GridWidget']]],
  ['setfront',['setFront',['../class_grid_widget.html#abe71249d6d54e84698dcb831c9df4c17',1,'GridWidget']]],
  ['setgridsize',['setGridSize',['../class_grid_widget.html#a87a46b6a3e5522e851732093b757baae',1,'GridWidget']]],
  ['setredo',['setRedo',['../class_dialog.html#a7515d0b8d86ba9a798d0aa5e1ea83a2f',1,'Dialog']]],
  ['settool',['setTool',['../class_grid_widget.html#a242812c406b269657eb6092af4a0ae7e',1,'GridWidget']]],
  ['settoolsize',['setToolSize',['../class_grid_widget.html#aca5c4e4edab002da08a7d76b7acf8ce7',1,'GridWidget']]],
  ['setundo',['setUndo',['../class_dialog.html#abab21e66c8b496296fccb401d20c229d',1,'Dialog']]],
  ['square',['square',['../class_grid_widget.html#a3d25401530305223417a0a0313e7105b',1,'GridWidget']]],
  ['switchoff',['switchOff',['../class_grid_widget.html#aca84a4a79a25f1adebaeb69f32022882',1,'GridWidget']]],
  ['switchon',['switchOn',['../class_grid_widget.html#ace3447d5fc329c47b55395a52a9eaf13',1,'GridWidget']]]
];
